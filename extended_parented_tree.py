import string
from copy import deepcopy
from nltk.tree import Tree, ParentedTree
from tree import Node as PyGramNode
from test_tree import Node as ZSSNode


class ExtendedParentedTree(ParentedTree):

    def __init__(self, *args, **kwargs):
        super(ExtendedParentedTree, self).__init__(*args, **kwargs)
        self.label = self.node
        self.children = self[0:]
        

    def add_left_sibling(self, new_node):
        parent = self._parent
        if parent is not None:
            parent_index = parent.treeposition
            index_on_parent = self.parent_index

            index_to_insert_at = index_on_parent
            parent.insert(index_to_insert_at, new_node)


    def add_right_sibling(self, new_node):
        parent = self._parent
        if parent is not None:
            parent_index = parent.treepositions
            index_on_parent = self.parent_index

            index_to_insert_at = index_on_parent + 1
            parent.insert(index_to_insert_at, new_node)


    def addkid(self, node, before=False):
        if before:
            self.insert(0, node)
        else:
            self.append(node)
        return self


    def pprint(self, margin=70, indent=0, nodesep='', parens='()', quotes=False):
        # Try writing it on one line.
        s = self._pprint_flat(nodesep, parens, quotes)
        if len(s)+indent < margin:
            return s

        # If it doesn't fit on one line, then write it on multi-lines.
        if isinstance(self.node, basestring):
            s = '%s%s%s' % (parens[0], self.node, nodesep)
        else:
            s = '%s%s%s' % (parens[0], self.node['pos'], nodesep)
        for child in self:
            if isinstance(child, Tree):
                s += '\n'+' '*(indent+2)+child.pprint(margin, indent+2,
                                                      nodesep, parens, quotes)
            elif isinstance(child, tuple):
                s += '\n'+' '*(indent+2)+ "/".join(child)
            elif isinstance(child, basestring) and not quotes:
                s += '\n'+' '*(indent+2)+ '%s' % child
            else:
                s += '\n'+' '*(indent+2)+ '%r' % child
        return s+parens[1] 


    def _pprint_flat(self, nodesep, parens, quotes):
        childstrs = []
        for child in self:
            if isinstance(child, Tree):
                childstrs.append(child._pprint_flat(nodesep, parens, quotes))
            elif isinstance(child, tuple):
                childstrs.append("/".join(child))
            elif isinstance(child, basestring) and not quotes:
                childstrs.append('%s' % child)
            else:
                childstrs.append('%r' % child)
        if isinstance(self.node, basestring):
            return '%s%s%s %s%s' % (parens[0], self.node, nodesep,
                                    string.join(childstrs), parens[1])
        else:
            return '%s%s%s %s%s' % (parens[0], self.node['pos'], nodesep,
                                    string.join(childstrs), parens[1]) 


def move_to_left_of_parent(tree, tree_position):
    # TODO need to handle parent = root
    node_to_move = tree[tree_position]
    parent = node_to_move._parent
    grandparent = parent._parent if parent is not None else None
    if parent is not None and grandparent is not None:
        index_on_parent = node_to_move.parent_index
        index_on_grandparent = parent.parent_index

        index_to_insert_at = index_on_grandparent
        popped_node = parent.pop(index_on_parent)
        grandparent.insert(index_to_insert_at, popped_node)


def move_to_right_of_parent(tree, tree_position):
    # TODO need to handle parent = root
    node_to_move = tree[tree_position]
    parent = node_to_move._parent
    grandparent = parent._parent if parent is not None else None
    if parent is not None and grandparent is not None:
        index_on_parent = node_to_move.parent_index
        index_on_grandparent = parent.parent_index

        index_to_insert_at = index_on_grandparent + 1
        popped_node = parent.pop(index_on_parent)
        grandparent.insert(index_to_insert_at, popped_node)


def remove_parent_and_move_children_up(tree, parent_position):
    node_to_delete = tree[parent_position]
    node_parent = node_to_delete._parent
    if node_parent is not None:
        index_to_insert_at = node_to_delete.parent_index

        node_children = node_to_delete[0:]
        num_children = len(node_children)
        popped_children = list()
        for i in range(num_children-1, -1, -1):
            popped_children.append(node_to_delete.pop(i))
        del tree[parent_position]
        for child in popped_children:
            node_parent.insert(index_to_insert_at, child)


def move_to_be_leftmost_child_of_right_sibling(tree, tree_position):
    # TODO need to handle parent = root
    node_to_move = tree[tree_position]
    parent = node_to_move._parent
    right_sibling = node_to_move.right_sibling
    if parent is not None and right_sibling is not None:
        index_on_parent = node_to_move.parent_index
        index_to_insert_at = 0

        popped_node = parent.pop(index_on_parent)
        right_sibling.insert(index_to_insert_at, popped_node)


def move_to_left_of_given_ancestor(tree, tree_position, ancestor_position):
    # TODO need to handle parent = root
    node_to_move = tree[tree_position]
    parent = node_to_move._parent
    ancestor = tree[ancestor_position]
    grandancestor = ancestor._parent if ancestor is not None else None
    if ancestor is not None and grandancestor is not None:
        index_on_parent = node_to_move.parent_index
        index_on_grandancestor = ancestor.parent_index

        index_to_insert_at = index_on_grandancestor
        popped_node = parent.pop(index_on_parent)
        grandancestor.insert(index_to_insert_at, popped_node)


def move_to_right_of_given_ancestor(tree, tree_position, ancestor_position):
    # TODO need to handle parent = root
    node_to_move = tree[tree_position]
    parent = node_to_move._parent
    ancestor = tree[ancestor_position]
    grandancestor = ancestor._parent if ancestor is not None else None
    if ancestor is not None and grandancestor is not None:
        index_on_parent = node_to_move.parent_index
        index_on_grandancestor = ancestor.parent_index

        index_to_insert_at = index_on_grandancestor + 1
        popped_node = parent.pop(index_on_parent)
        grandancestor.insert(index_to_insert_at, popped_node)


def add_parent(tree, tree_position):
    node_to_add_parent = tree[tree_position]
    parent = node_to_add_parent._parent
    if parent is not None:
        index_on_parent = node_to_add_parent.parent_index
        new_node = node_to_add_parent.copy(deep=True)
        new_node.node = deepcopy(node_to_add_parent.node)
        node_to_add_parent[0:] = [new_node]


def find_highest_tree_with_node_as_leftmost(tree, tree_position):
    leaf = tree[tree_position]
    containing_treeposition = tree_position
    current_position = tree_position[:-1]
    for i in range(len(tree_position[:-1])-1):
        contained_leaves = tree[current_position].leaves()
        if contained_leaves[0] == leaf:
            containing_treeposition = current_position
            current_position = current_position[:-1]
        else:
            break
    return containing_treeposition


def find_highest_tree_with_node_as_rightmost(tree, tree_position):
    leaf = tree[tree_position]
    containing_treeposition = tree_position
    current_position = tree_position[:-1]
    for i in range(len(tree_position[:-1])-1):
        contained_leaves = tree[current_position].leaves()
        if contained_leaves[-1] == leaf:
            containing_treeposition = current_position
            current_position = current_position[:-1]
        else:
            break
    return containing_treeposition


def subtree_only_contains_given_tags(tree, tree_position, tags):
    relevant_subtree = tree[tree_position]
    all_positions = relevant_subtree.treepositions(order='preorder')
    relevant_positions = sorted(all_positions, key=len)[1:]
    for subtree_position in relevant_positions:
        subtree_node = relevant_subtree[subtree_position]
        if not isinstance(subtree_node, basestring):
            subtree_pos = subtree_node.node['pos']
            if subtree_pos not in tags:
                return False
    return True


def flatten_subtree(tree, tree_position):
    head_node = tree[tree_position]
    leaf_positions = head_node.treepositions(order='leaves')
    preterminals = set([x[:-1] for x in leaf_positions])
    nodes_with_leaves = list()
    for tree_pos in reversed(sorted(preterminals)):
        leaf_parent = head_node[tree_pos]
        leaf_grandparent = leaf_parent._parent
        index_on_grandparent = leaf_parent.parent_index
        nodes_with_leaves.append(leaf_grandparent.pop(index_on_grandparent))

    # pop off all children of original node
    head_node_children = head_node[0:]
    num_children = len(head_node_children)
    for i in range(num_children-1, -1, -1):
        head_node.pop(i)

    for new_child in reversed(nodes_with_leaves):
        head_node.append(new_child)


def strip_subtrees_from_positions(all_positions, tree_position):
    new_positions = list()
    len_pos = len(tree_position)
    for pos in all_positions:
        if pos[:len_pos] == tree_position[:]:
            continue
        else:
            new_positions.append(pos)
    return new_positions


def strip_preterminals_from_positions(all_positions, leaf_positions):
    new_positions = deepcopy(all_positions)
    leaf_preterminals = [x[:-1] for x in leaf_positions]
    for preterminal in leaf_preterminals:
        indices_to_remove = list()
        len_pos = len(preterminal)
        for idx, pos in enumerate(new_positions):
            if pos[:len_pos] == preterminal[:]:
                indices_to_remove.append(idx)
            else:
                continue
        for idx in reversed(indices_to_remove):
            new_positions.pop(idx)
    return new_positions


def convert_to_pygram_node_tree(tree_node):
    # assuming original is ExtendedParentedTree and new is PyGramNode
    if isinstance(tree_node, ExtendedParentedTree):
        node = PyGramNode(tree_node.node)
        for child in tree_node[0:]:
            node.children.append(convert_to_pygram_node_tree(child))
        return node
    else:
        return PyGramNode(tree_node)


def convert_to_zss_node_tree(tree_node):
    # assuming original is ExtendedParentedTree and new is PyGramNode
    if isinstance(tree_node, ExtendedParentedTree):
        node = ZSSNode(tree_node.node)
        for child in tree_node[0:]:
            node.children.append(convert_to_zss_node_tree(child))
        return node
    else:
        return ZSSNode(tree_node)


# Tree is
#           A
#           B
#      C        D
#      c     E      F
#            e   G     H
#                g     h
"""
test_tree = ExtendedParentedTree('(A (B (C c) (D (E e) (F (G g) (H h)))))')
print 'Tree after insertion is %s' % test_tree.pprint_latex_qtree()
output_tree = convert_to_node_tree(test_tree)
print output_tree
print [x.label for x in output_tree.children]
positions = test_tree.treepositions(order='preorder')
leaves = test_tree.treepositions(order='leaves')
print positions
position = positions[-1][:-1]
print position, test_tree[position]
new_node = test_tree[position].copy(deep=True)
test_tree[position].addkid(new_node)
print 'Tree after insertion is %s' % test_tree.pprint_latex_qtree()
add_parent(test_tree, position)
print 'Tree after insertion is %s' % test_tree.pprint_latex_qtree()
print strip_preterminals_from_positions(positions, leaves)
print strip_subtrees_from_positions(positions, position)
flatten_subtree(test_tree, position[:-3])
arching_pos = find_highest_tree_with_node_as_rightmost(test_tree, position)
print 'position %s, arching %s' % (position, test_tree[arching_pos])
move_to_right_of_given_ancestor(test_tree, position[:-1], arching_pos)
print 'Tree is %s' % test_tree.pprint_latex_qtree()
leaf_positions = test_tree.treepositions(order='leaves')
leftmost_leaf = test_tree[leaf_positions[0][:-1]]
left_middle_leaf = test_tree[leaf_positions[1][:-1]]
right_middle_leaf = test_tree[leaf_positions[2][:-1]]
rightmost_leaf = test_tree[leaf_positions[3][:-1]]
print 'Leftmost leaf is %s' % leftmost_leaf
print 'Left middle leaf is %s' % left_middle_leaf
print 'Right middle leaf is %s' % right_middle_leaf
print 'Rightmost leaf is %s' % rightmost_leaf
new_node_1 = ExtendedParentedTree('(X x)')
new_node_2 = ExtendedParentedTree('(Y y)')
new_node_3 = ExtendedParentedTree('(Z z)')
leftmost_leaf.add_left_sibling(new_node_1)
rightmost_leaf.add_right_sibling(new_node_2)
#move_to_right_of_parent(test_tree, leaf_positions[3][:-1])
print 'Tree after insertion is %s' % test_tree.pprint_latex_qtree()
"""
