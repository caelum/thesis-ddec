from nltk.tree import Tree, ParentedTree

s = '(A (B (D (E (F (G h)) (R (S t))))) (C (I (J (K (L m))))))'

tree = Tree(s)

def modified_collapse_unary(tree, collapsePOS = False, collapseRoot = False, joinChar = "+"):
    if collapseRoot == False and isinstance(tree, Tree) and len(tree) == 1:
        nodeList = [tree[0]]
    else:
        nodeList = [tree]

    # depth-first traversal of tree
    while nodeList != []:
        node = nodeList.pop()
        if isinstance(node, Tree):
            if len(node) == 1 and isinstance(node[0], Tree) and (collapsePOS == True or isinstance(node[0,0], Tree)):
                #node.node = node[0].node #+= joinChar + node[0].node
                node[0:] = [child for child in node[0]]
                # since we assigned the child's children to the current node,
                # evaluate the current node again
                nodeList.append(node)
            else:
                for child in node:
                    nodeList.append(child)


modified_collapse_unary(tree, collapsePOS=True)
print tree.pos()
print tree
